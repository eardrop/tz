 "use strict"
 angular.module('app')
    .run(function($httpBackend) {

        var goods = [{
            id: 1,
            name: 'Apple',
            photo: 'http://weknowyourdreamz.com/images/apple/apple-01.jpg',
            price: 0.6
        }, {
            id: 2,
            name: 'Bike',
            photo: 'http://www.bikehiredirect.com/images/pix/MaleBike1.png',
            price: 230
        }, {
            id: 3,
            name: 'Phone',
            photo: 'https://openclipart.org/download/213897/black-android-phone.svg',
            price: 160
        }, {
            id: 4,
            name: 'Pen',
            photo: 'http://www.thewritingdesk.co.uk/sailor/kingofpens_crosspoint.jpg',
            price: 12
        }, {
            id: 5,
            name: 'Tyson',
            photo: 'http://static.comicvine.com/uploads/original/11120/111202779/4284356-mike_tyson_061009_003.jpg',
            price: 1200
        }];

        var reviews = [{
            name: 'Sam',
            date: '24/02/1994',
            text: 'John Doe is sometimes used to refer to a typical male in other contexts as well, in a similar manner to John Q. Public in the United States or Joe Public, John Smith or Joe Bloggs in Britain. For example, the first name listed on a form might be John Doe, along with a fictional address or other fictional information to provide an example of how to fill in the form.',
          },{
              name: 'Smith',
              date: '24/02/1994',
              text: 'John Doe is sometimes used to refer to a typical male in other contexts as well, in a similar manner to John Q. Public in the United States or Joe Public, John Smith or Joe Bloggs in Britain. For example, the first name listed on a form might be John Doe, along with a fictional address or other fictional information to provide an example of how to fill in the form.',
          },{
              name: 'Joe',
              date: '24/02/1994',
              text: 'John Doe is sometimes used to refer to a typical male in other contexts as well, in a similar manner to John Q. Public in the United States or Joe Public, John Smith or Joe Bloggs in Britain. For example, the first name listed on a form might be John Doe, along with a fictional address or other fictional information to provide an example of how to fill in the form.',
          }];

        $httpBackend.whenGET(/\.html/).passThrough();
        $httpBackend.whenGET('/reviews/').respond(reviews);
        $httpBackend.whenPOST('/add/review/').respond(function(method, url, data, headers){
          reviews.push({
            name: angular.fromJson(data).name,
            text: angular.fromJson(data).text,
            date: new Date()
          })
          return [200, reviews];
        });

        $httpBackend.whenGET('/goods/').respond(goods);
        var id = null;
        $httpBackend.whenGET(function(url) {
          return id = parseInt(url.split('/')[2]);
        }).respond(function() {
          if (id) {
            return [200, goods[id - 1]];
          } else {
            return [404, {}];
          }
        });
    });
