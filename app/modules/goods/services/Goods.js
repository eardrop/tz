goodsApp.service("GoodsService", ['$http', function($http) {

  this.getList = function() {
    return $http.get('/goods/')
  }

  this.getOne = function(id) {
    return $http.get('/goods/' + id)
  };
}]);
