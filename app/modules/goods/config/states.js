"use strict"
goodsApp.config(function estateConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('goods', {
                url: '/goods',
                controller: 'GoodsCtrl',
                templateUrl: 'app/modules/goods/views/goods.html'
            })
            .state('goods.showOne', {
                url: "/:id",
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: "app/modules/goods/views/modals/goods-one.html",
                        controller: "GoodsOneCtrl"
                    }).result.finally(function() {
                        $state.go('^');
                    });
                }]
            })
    });
