goodsApp.controller('GoodsOneCtrl', function($scope, $stateParams, $http, GoodsService) {

  GoodsService.getOne($stateParams.id).success(function(data) {
    $scope.item = data;
  });

  $scope.close = function() {
      $scope.$close(true);
  };
});
