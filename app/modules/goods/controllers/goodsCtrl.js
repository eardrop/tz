"use strict"
goodsApp.controller('GoodsCtrl', function($scope,  $modal, $http, GoodsService) {

  $scope.order = 'id';

  GoodsService.getList().success(function(data) {
    $scope.data = data;
  });
});
