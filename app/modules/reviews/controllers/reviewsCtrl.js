reviewsApp.controller('ReviewsCtrl', function ($scope, $http, ReviewsService) {

  var asignData = function() {
    $scope.data = ReviewsService.items;
  }

  $scope.$watch(function() {
    return ReviewsService.a
  }, function(current, previous){
    $scope.a = current
  });

  $scope.$watch(function() {
    return ReviewsService.items
  }, function(current, previous){
    $scope.data = current
  });

  ReviewsService.getData().then(asignData);

  $scope.submitForm = function(name, text) {
    ReviewsService.addData(name, text)
    .then(asignData);
  }
});
