reviewsApp.config(function estateConfig($stateProvider) {
	$stateProvider
	  .state('reviews', {
	    url: '/reviews',
      controller: 'ReviewsCtrl',
      templateUrl: 'app/modules/reviews/views/reviews.html'
	  })
	}
);
