app.service('ReviewsService', function($q, $interval, $http) {
  var svc = {};
  svc.items = [];

  svc.a = 0
  $interval(function() {
    svc.a = Math.random()
  }, 500);

  svc.getData = function() {
    return $http.get('/reviews/').success(function(data) {
      svc.items = data;
    })
  };

  svc.addData = function(name, text) {
    return $http.post('/add/review/', {name, text}).success(function(data) {
      svc.items = data;
    });
  }
  return svc;
});
